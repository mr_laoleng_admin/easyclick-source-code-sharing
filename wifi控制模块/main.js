//在src下建立slib目录,复制wifi.js过去
var wifiUtils = require("slib/WIFI");

//普通用法
if (wifiUtils.isWifiEnable()) {
    var ssid = wifiUtils.isConnectedWifi();
    switch (ssid) {
        //线上环境切换到灰度
        case '"WAM-WIFI"' : {
            wifiUtils.connectWifiPws("DYTEST", "meimimahaha");
            toastLog("已经切换到DYTEST");
            break;
        }
        //灰度环境切换到线上
        case '"DYTEST"' : {
            wifiUtils.connectWifiPws("WAM-WIFI", "fHfdP4RR");
            toastLog("已经切换到WAM");
            break;
        }
    }
}

//代理用法
if (wifiUtils.isProxyed()) {
    wifiUtils.unSetHttpProxy();
    toastLog("代理已清除");
} else {
    wifiUtils.setHttpPorxySetting("192.168.1.13", 8888);
    toastLog("已设置代理为: 192.168.1.13");
}
