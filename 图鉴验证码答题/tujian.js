/**
 * @author Mr_老冷 QQ:1920712147
 * @description EasyClick教学咨询 688元VIP提高班
 */
let TUJIAN = (function () {
    function TUJIAN(softid) {
        this._uplodHost = "http://api.ttshitu.com/predict"
        this._errerHost = "http://api.ttshitu.com/reporterror.json"
        this._softid = softid || "409116c232d0412584f40366c396fe8f"
        this._userName = null
        this._password = null
        this._codeId = null
        this.debug = true
    }

    //设置账户
    TUJIAN.prototype.SetAccount = function (uesr, pass) {
        this._userName = uesr
        this._password = pass
    }
    //识别
    TUJIAN.prototype.requestData = function (type, img) {
        if (!this._userName || !this._password) {
            this.Debug("请初始化账号")
            return false
        }
        // http.__okhttp__.setTimeout(60000)
        this._codeId = null
        let ret = null
        let bool = false
        let json = {}
        json.username = this._userName
        json.password = this._password
        json.typeid = type
        json.softid = this._softid
        json.image = image.toBase64Format(img, "png", 50)
        try {
            ret = http.httpPost(this._uplodHost, json, null, 60 * 1000, null)
            ret = JSON.parse(ret)
            bool = ret.code == 0 && ret.success == true
        } catch (e) {
            this.Debug("请求出错：", e)
            return false
        }
        image.recycle(img)//回收图片

        if (bool) {
            this._codeId = ret.data.id
            return {
                "code": 0,
                "result": ret.data.result
            }
        }
        return {"code": ret.code, "result": ret.message}
    }

    //上报错误
    TUJIAN.prototype.ReportAnError = function () {
        if (!this._codeId) return this.Debug("请在识别图片后且结果不准确时调用！")
        try {
            let ret = http.httpPost(this._errerHost, {
                id: this._codeId
            })
            if (ret) {
                ret = JSON.parse(ret)
                return ret.code == 0 ? ret.data.result : ret.message
            }
        } catch (e) {
            return "请求出错：" + e
        }
    }
    //调试输出
    TUJIAN.prototype.Debug = function (str) {
        if (this.debug) console.log(str)
    }
    return TUJIAN
})()

