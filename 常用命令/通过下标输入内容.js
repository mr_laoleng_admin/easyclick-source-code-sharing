/**
 * @author Mr_老冷 QQ1920712147
 * @description 通过下标输入内容
 * @param selector{S} 选择器
 * @param content{string} 输入内容
 * @param index{number?} 下标,从0开始,可空
 */
function inputTextEx(selector, content, index) {
    index = index || 0
    let nodes = selector.getNodeInfo(0)
    if (nodes) {
        if (nodes.length - 1 < index) index = nodes.length - 1
        return nodes[index].inputText(content)
    }
    return false
}


let slt = text("请输入")
//下标从0开始
inputTextEx(slt, "你想输入什么", 1)