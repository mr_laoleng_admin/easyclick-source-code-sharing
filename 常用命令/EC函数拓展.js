//丢到main同级目录
NodeInfo.prototype.centerX = function () {
    return ~~((this.bounds.left + this.bounds.right) / 2)
}
NodeInfo.prototype.centerY = function () {
    return ~~((this.bounds.top + this.bounds.bottom) / 2)
}
Rect.prototype.centerX = function () {
    return ~~((this.left + this.right) / 2)
}
Rect.prototype.centerY = function () {
    return ~~((this.top + this.bottom) / 2)
}

//用法
// let obj = text("云服务").getOneNodeInfo(0)
// logd(JSON.stringify(obj.bounds));
// logd(obj.centerX());
// logd(obj.centerY());
// sleep(500)
// logd(obj.bounds.centerX());
// logd(obj.bounds.centerY());
// sleep(500)
// exit()