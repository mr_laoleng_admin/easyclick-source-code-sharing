/**
 * @author Mr_老冷 QQ:1920712147
 * @date 20210515
 */
laoleng = {}
laoleng.Bytes = {}
laoleng.String = {}
/**
 * @description base64解密,返回bytes
 * @param {String} str "YWJj"
 * @return {bytes[]} bytes
 */
laoleng.Bytes.base64Decode = function (str) {
    importClass(android.util.Base64)
    return Base64.decode(str, Base64.DEFAULT)
}
/**
 * @description 对bytes进行base64加密,返回base64结果
 * @param {bytes[]} bytes [B@8a3bd58 待加密的字节集
 * @return {String} base64编码文字 "YWJj"
 */
laoleng.Bytes.base64Encode = function (bytes) {
    importClass(android.util.Base64)
    return Base64.encodeToString(bytes, Base64.DEFAULT);
}
/**
 * @description base64加密
 * @param {String} str "abc"
 * @return {String} string "YWJj"
 */
laoleng.String.base64Encode = function (str) {
    importClass(android.util.Base64)
    return Base64.encodeToString(this.stringtoBytes(str), Base64.NO_WRAP)
}
/**
 * @description base64解密,返回字符串
 * @param {String} str "YWJj"
 * @return {String} string "abc"
 */
laoleng.String.base64Decode = function (str) {
    importClass(android.util.Base64)
    return this.byteToString(Base64.decode(str, Base64.DEFAULT))
}
/**
 * @description 字符串转bytes
 * @param {String} str 源字符串 "abc"
 * @return {bytes[]} bytes [B@8a3bd58
 */
laoleng.String.stringtoBytes = function (str) {
    return new java.lang.String(str).getBytes()
}
/**
 * @description bytes转字符串
 * @param {bytes[]} bytes [B@8a3bd58
 * @return {String} string  "abc"
 */
laoleng.String.byteToString = function (bytes) {
    return new java.lang.String(bytes)
}