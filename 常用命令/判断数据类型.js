/**
 * @author Mr_老冷 QQ:1920712147
 * @description 判断数据类型
 * @param arg{any}
 * @return {string|null|undefined}
 */
function typeOf(arg) {
    if (arg === null) return null
    if (arg === undefined) return undefined
    return arg.constructor.name
}
let data = device.tcDeviceId()
logd(typeof data)
//object
logd(typeOf(data));
//String